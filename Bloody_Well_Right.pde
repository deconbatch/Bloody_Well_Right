/**
 * Bloody Well Right.
 * Wet and organic images from the inorganic formula.
 * ref. https://twitter.com/ntsutae/status/1349018828907413506
 *
 * @author @deconbatch
 * @version 0.1
 * @license GPL Version 3 http://www.gnu.org/licenses/
 * Processing 3.5.3
 * 2021.01.17
 */

void setup() {
  size(980, 980);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  rectMode(CENTER);
  smooth();
  noLoop();
}

void draw() {
  int   frmMax  = 3;
  int   hW      = floor(width * 0.7);
  int   hH      = floor(height * 0.7);
  float hueBase = random(360.0);
  float complex = 0.3;  // 0.3-1.0

  translate(width * 0.5, height * 0.5);
  for(int frmCnt = 0; frmCnt < frmMax; frmCnt++) {
      
    float xRatio = random(0.005, 0.01);
    float yRatio = random(0.005, 0.01);
    
    pushMatrix();
    rotate(floor(random(4.0)) * HALF_PI * 0.5);
    background(hueBase % 360.0, 10.0, 100.0, 100.0);
    noStroke();
    for (int x = -hW; x < hW; x++) {
      for (int y = -hH; y < hH; y++) {
        float x0 = funcX(x * xRatio, y * yRatio, complex);
        float y0 = funcY(x * xRatio, y * xRatio, complex);
        float x1 = funcX(x0, y0, complex);
        float y1 = funcY(x0, y0, complex);
        float x2 = funcX(x1, y1, complex);
        float y2 = funcY(x1, y1, complex);

        float rHue = (hueBase + 360.0 + ((x1 - y1) * 10.0) % 360.0) % 360.0;
        float rSat = 80.0 - abs((x0 + y0) * 50.0) % 60.0;
        float rBri = 90.0 - abs((x1 + y1) * 5.0) % 90.0;
        float rAlp = 100.0 - abs((x0 + y0) * 20.0) % 100.0;
        fill(rHue, rSat, rBri, rAlp);
				rect(x, y, 1.0, 1.0);
      }
    }  
    popMatrix();

    casing();
    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");

    hueBase += 360.0 / frmMax;
    complex += 0.35;
  }
  exit();
}

/**
 * funcX : shape calculate function.
 */
float funcX(float _a, float _b, float _c) {
  return (cos(_a * 3.0) - sin(_b * 4.0)) * _c;
}

/**
 * funcY : shape calculate function.
 */
float funcY(float _a, float _b, float _c) {
  return (_a * _a - _b * _b - _a * _b) * _c;
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 50.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
  noStroke();
}
